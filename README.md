### Exercício 
Dentro de uma sistema comercial, temos que uma pessoa física possui varias contas a pagar. Crie cada uma das classes com, pelo menos os respectivos métodos:  `preencher()` , `imprimir()` e `copiar()` 

Use o relacionamento de agregação para o relacionamento entre Pessoa e Contas**.** Lembre-se, sempre que necessário usar a abordagem 03 (delegação) que falamos acima.

![UML](./src/main/resources/images/UML-PessoaFisica-Contas.png)

Finalmente, crie na função main uma lista com 5 pessoas e cada um com três contas para pagar. Depois crie uma função que verifique qual a pessoa que tem o maio saldo devedor e qual o total devido por todas as pessoas.


## Getting started
- Clone o projeto e estude em seu computador
- Atenção pois estamos utilizando MAVEN
    - Logo todas as configurações necessárias deverão ser feitas no arquivo pom.xml
    - Neste caso estou usando o java 11.
    - Caso você esteja usando outra versao, abra o pom.xml e altere para a sua versão   
- Faça algumas alterações e teste, assim que a gente aprende
- Finalmente, tente melhorá-lo e criar algumas funcionalidades na função main.


### Menu de funcionamento do programa
    
    ```------- Sistema PessoaFisica e suas contas a pagar -------
	1 - Inserir Pessoa
	2 - Remover Pessoa
	3 - Inserir conta a uma pessoa
	4 - Pesquisar Pessoa (cpf)
	5 - Pesquisar Pessoa (nome)
	6 - Pessoa com maior devedor 
	7 - Total devido por todas pessoas 
	0 - Sair
      ----------------------------------------
    ```
